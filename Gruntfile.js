module.exports = function(grunt) {

  // Configuration goes here
grunt.initConfig({
    sass: {
        dist: {
          files: [ // Compile sass files to css files
            {
              expand: true,
              cwd: 'sass/',
              src: ['main.scss'],
              dest: 'css/',
              ext: '.css'
            }
          ]
        }
    },
    watch: {
      sass: {
        files: "sass/main.scss",
        tasks: ['sass']
      }
    }
  });

  // Load plugins here
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // Define your tasks here
  grunt.registerTask('default',['sass']);
  grunt.registerTask('compile',['sass','watch']);
};