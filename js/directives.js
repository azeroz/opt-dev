var app = angular.module('directives', []);

app.directive('imageUploader', [ function() {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'partials/uploader.html',
        link: function(scope,element,attrs){
        	scope.config = {};
        	var uploadType = function(){
        		if (attrs.uploadType == 'single'){
        			scope.config.singleFile = true;
        		}
        		else if (attrs.uploadType == 'multiple'){
        			scope.config.singleFile = false;
        		}
        	};
        	uploadType();
        }
    };
}]);