var app = angular.module('controllers', []);

app.controller('dataCtrl', ['$scope','$http', function ($scope,$http) {
	function DataList(){
		this.add = function(){
			return this.dl.push({
				"content": ""
			});
		};
		this.delete = function(index){
			return this.dl.splice(index,1);
		};
		this.getData = function(){
			this.dl = angular.forEach(this.dl, function(value,key){
				value.content = ($('.data-input > input')[key].value);
			});
		};
		this.save = function(){
			this.getData();
			console.log(JSON.stringify(this.dl));
			// save data to API
		};
	};
	$scope.dataList = new DataList();
	$http.get('partials/data.json')
		.success(function(data){
			$scope.dataList.dl = data;
		});	
}]);